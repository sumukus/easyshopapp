import { View, Text, FlatList, StyleSheet } from "react-native";
import { useState, useEffect } from "react";
import ProductList from "./ProductList";

const data = require("../../assets/data/products.json");

function ProductContainer() {
  const [products, setProducts] = useState();

  useEffect(() => {
    setProducts(data);
    return () => {
      setProducts([]);
    };
  }, []);
  return (
    <View style={styles.rootContainer}>
      {/* <Text style={styles.title}>Product Container</Text> */}
      <View>
        <FlatList
          numColumns={2}
          horizontal={false}
          data={products}
          renderItem={({ item }) => <ProductList item={item} />}
          keyExtractor={(item) => item._id.$oid}
          contentContainerStyle={{ paddingBottom: 60 }}
        />
      </View>
    </View>
  );
}

export default ProductContainer;

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: "gainsboro",
  },
  // title: {
  //   padding: 10,
  //   fontWeight: "bold",
  // },
});
