import { TouchableOpacity, StyleSheet, View, Dimensions } from "react-native";
import ProductCard from "./ProductCard";

function ProductList({ item }) {
  return (
    <TouchableOpacity style={styles.rootContainer}>
      <View style={styles.body}>
        <ProductCard product={item} />
      </View>
    </TouchableOpacity>
  );
}
export default ProductList;

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  rootContainer: {
    width: "50%",
  },
  body: {
    width: width / 2,
  },
});
