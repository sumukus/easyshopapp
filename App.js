import { StatusBar } from "expo-status-bar";
import { StyleSheet, View } from "react-native";
import ProductContainer from "./screens/Products/ProductContainer";
import Header from "./Shared/Header";
import { SafeAreaView } from "react-native-safe-area-context";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />
      <Header />
      <ProductContainer />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
